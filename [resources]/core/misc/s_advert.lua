
local ad_price = 200;
function send_advertise(plr, cmd, ...)
	if (isGuestAccount(getPlayerAccount(plr))) then
		return
	end
	local m = getPlayerMoney(plr)
	if (m < ad_price) then
		local money = tostring(ad_price - m)
		return exports.topbar:dm("Você precisa R$"..money.."", plr, 255,0,0, true)
	end
	local msg = table.concat({ ... }, " ")
	if msg == "" then
		return exports.topbar:dm("Você não digitou uma msg", plr, 255,0,0, true)
	end
	takePlayerMoney(plr, ad_price)
	exports.topbar:dm("R$"..ad_price.." foi deduzido para o anúncio", plr, 255,100,0, true)
	outputChatBox("#FFFF00[Anúncio] "..getPlayerName(plr)..": #FFFFFF"..msg, root, 255,0,0, true)
end
addCommandHandler("ad", send_advertise)
addCommandHandler("advertise", send_advertise)
addCommandHandler("anuncio", send_advertise)
addCommandHandler("anunciar", send_advertise)
addCommandHandler("advert", send_advertise)
