
local enable_flying_cars = nil
function toggle_flying_cars()
	local is_admin = exports.staff:isAdmin(localPlayer)
	if is_admin then
	    	if not enable_flying_cars then
	            	enable_flying_cars = true
	            	setWorldSpecialPropertyEnabled("aircars", true)
			exports.topbar:dm("Flying cars: enabled", 0, 200, 0)
	        else
	            	enable_flying_cars = false
	            	setWorldSpecialPropertyEnabled("aircars", false)
			exports.topbar:dm("Flying cars: disabled", 0, 200, 0)
	        end
	else
	        exports.topbar:dm("Only admins can do that!", 200, 0, 0)
	end
end
addCommandHandler("carsfly", toggle_flying_cars)
addCommandHandler("voarcar", toggle_flying_cars)
