--[[
********************************************************************************
	Project owner:		RageQuit community
	Project name: 		TopCity-RP
	Developers:   		Mr_Moose

	Source code:		https://github.com/404rq/TopCity-RP/
	Bugtracker: 		https://discuss.404rq.com/t/issues
	Suggestions:		https://discuss.404rq.com/t/development

	Version:    		Open source
	License:    		BSD 2-Clause
	Status:     		Stable release
********************************************************************************
]]--

local default_money = 428  		-- Global definition based on what mechanics earn IRL/year
					-- divided by 12 and then 31 as in days multiplied by 4

--[[ Repair vehicle with 100% health ]]--
function big_repair(veh)
	fixVehicle(veh)
	setElementHealth(veh, getElementHealth(veh))
end

--[[ Pay for repair and refuel ]]--
function pay_repair(mech, owner)
	local pacc = getPlayerAccount(mech)
	local repaired_cars = exports.core:get_account_data(pacc, "GTWdata.stats.repaired_cars") or 0
	if isElement(owner) then
		takePlayerMoney(owner, default_money*3)
	end
	givePlayerMoney(mech, (default_money*2)+repaired_cars)
end
function pay_refuel(mech, owner)
	local pacc = getPlayerAccount(mech)
	local repaired_cars = exports.core:get_account_data(pacc, "GTWdata.stats.repaired_cars") or 0
	if owner and isElement(owner) then
		takePlayerMoney(owner, default_money)
	end
	givePlayerMoney(mech, (default_money/2)+repaired_cars)
end

--[[ Repair vehicle as mechanic ]]--
function repair_veh(veh, repairTime)
	if not veh or not isElement(veh) then outPutTopbarMessage("Nenhum veículo encontrado", client, 255, 0, 0) return end
	local acc = getElementData(veh, "owner")
	local acc2,owner = nil,nil
	if acc then
		acc2 = getAccount(acc)
		owner = getAccountPlayer(acc2)
	end
	if not owner then outPutTopbarMessage("O proprietário deste veículo está atualmente offline", client, 255, 100, 0) end

	-- Freeze elements during repair
	setElementFrozen(veh, true)
	setElementFrozen(client, true)
	setPedAnimation(client, "GRAFFITI", "spraycan_fire", -1, true, false)
	exports.UI:showGUICursor(client, true)
	outPutTopbarMessage("Reparando...", client, 0, 255, 0)
	if owner then outPutTopbarMessage("Veículo foi reparado...", owner, 0, 255, 0) end

	-- Reset after repair
	setTimer(big_repair, math.floor(repairTime), 1, veh)
	--setTimer(exports.UI:showGUICursor, math.floor(repairTime), 1, client, false)
	setTimer(setElementFrozen, math.floor(repairTime), 1, veh, false)
	setTimer(setElementFrozen, math.floor(repairTime), 1, client, false)
	setTimer(outPutTopbarMessage, math.floor(repairTime), 1, "Veículo foi reparado com sucesso!", client, 0, 255, 0)
	if owner then setTimer(outPutTopbarMessage, math.floor(repairTime), 1, "Seu veículo foi reparado por: "..getPlayerName(client), owner, 0, 255, 0) end
	setTimer(pay_repair, math.floor(repairTime), 1, client, owner)
	setTimer(setPedAnimation, math.floor(repairTime), 1, client, nil, nil)

	-- Increase stats by 1 (if not your own car, solution to abuse 2014-11-13)
	if owner == client then return end
	local playeraccount = getPlayerAccount(client)
	local repaired_cars = exports.core:get_account_data(playeraccount, "GTWdata.stats.repaired_cars") or 0
	exports.core:set_account_data(playeraccount, "GTWdata.stats.repaired_cars", repaired_cars + 1)
end
addEvent("mechanic.repair", true)
addEventHandler("mechanic.repair", root, repair_veh)

--[[ Refule as mechanic ]]--
function refuel_veh(veh, refuelTime)
	if not veh or not isElement(veh) then outPutTopbarMessage("Nenhum veículo encontrado", client, 255, 0, 0) return end
	local acc = getElementData(veh, "owner")
	local acc2,owner = nil,nil
	if acc then
		acc2 = getAccount(acc)
		owner = getAccountPlayer(acc2)
	end
	if not owner then outPutTopbarMessage("O proprietário deste veículo está atualmente offline", client, 255, 100, 0) end

	-- Freeze elements during repair
	setElementFrozen(veh, true)
	setElementFrozen(client, true)
	setPedAnimation(client, "GRAFFITI", "spraycan_fire", -1, true, false)
	exports.UI:showGUICursor(client, true)
	outPutTopbarMessage("veículo está sendo reabastecido...", client, 0, 255, 0)
	if owner then outPutTopbarMessage("Seu veículo está sendo reabastecido...", owner, 0, 255, 0) end

	-- Reset after repair
	--setTimer(exports.UI:showGUICursor, math.floor(refuelTime), 1, client, false)
	setTimer(setElementFrozen, math.floor(refuelTime), 1, veh, false)
	setTimer(setElementFrozen, math.floor(refuelTime), 1, client, false)
	setTimer(outPutTopbarMessage, math.floor(refuelTime), 1, "Veículo foi reabastecido com sucesso!", client, 0, 255, 0)
	if owner then setTimer(outPutTopbarMessage, math.floor(refuelTime), 1, "Seu Veículo foi reabastecido com sucesso! por : "..getPlayerName(client), owner, 0, 255, 0) end
	setTimer(setPedAnimation, math.floor(refuelTime), 1, client, nil, nil)
	setTimer(pay_refuel, math.floor(refuelTime), 1, client, owner)
	setTimer(setElementData, math.floor(refuelTime), 1, veh, "vehicleFuel", 100)
end
addEvent("mechanic.refuel", true)
addEventHandler("mechanic.refuel", root, refuel_veh)

--[[ Fix and repair as staff ]]--
function staff_repair(veh)
	local is_staff = exports.staff:isStaff(client)
    	if not is_staff then
		outPutTopbarMessage("Você não tem permissão para usar este recurso!", client, 255, 0, 0)
    		return
    	end
	if not veh or not isElement(veh) then return end
	outPutTopbarMessage("Veículo foi reparado com sucesso!", client, 0, 255, 0)
	setElementData(veh, "vehicleFuel", 100)
	big_repair(veh)
	local x,y,z = getElementPosition(client)
	outputServerLog("ADMIN: "..getPlayerName(client).." has repaired and refuled a vehicle at: ["..math.floor(x)..","..math.floor(y)..","..math.floor(z).."]")
end
addEvent("mechanic.staff.repair", true)
addEventHandler("mechanic.staff.repair", root, staff_repair)

--[[ Enter any vehicle as staff ]]--
function staff_enter(veh)
	local is_staff = exports.staff:isStaff(client)
    	if not is_staff then
		outPutTopbarMessage("Você não tem permissão para usar este recurso!", client, 255, 0, 0)
    		return
    	end
	if not veh or not isElement(veh) then return end
	warpPedIntoVehicle(client, veh)
	local x,y,z = getElementPosition(client)
	outputServerLog("ADMIN: "..getPlayerName(client).." has warped into a vehicle at: 		["..math.floor(x)..","..math.floor(y)..","..math.floor(z).."]")
end
addEvent("mechanic.staff.enter", true)
addEventHandler("mechanic.staff.enter", root, staff_enter)

--[[ Staff destroy vehicle ]]--
function staff_destroy(veh)
	local accName = getAccountName(getPlayerAccount(client))
	if not (isObjectInACLGroup("user."..accName, aclGetGroup("Admin")) or
    	isObjectInACLGroup("user."..accName, aclGetGroup("Developer")) or
    	isObjectInACLGroup("user."..accName, aclGetGroup("Moderator"))) then
		outPutTopbarMessage("Você não tem permissão para usar este recurso!", client, 255, 0, 0)
    	return
    end
	if not veh or not isElement(veh) then return end
	if getElementData(veh,"owner") then
		outPutTopbarMessage("Veículo foi removido com sucesso!", client, 0, 255, 0)
		outputServerLog("VEH_ADMIN: Vehicle was removed at: "..getZoneName(getElementPosition(veh))..
			", by: "..getPlayerName(client).." owner was: "..getElementData(veh,"owner"))
	end

	-- Clean up if owned vehicle bought in shop
	triggerEvent("vehicleshop.onPlayerVehicleDestroy", root, veh, true)
end
addEvent("mechanic.destroy", true)
addEventHandler("mechanic.destroy", root, staff_destroy)
function outPutTopbarMessage(message, thePlayer, r, g, b)
	exports.topbar:dm(message, thePlayer, r, g, b)
end

addCommandHandler("gtainfo", function(plr, cmd)
	outputChatBox("[TopCity-RP] "..getResourceName(
	getThisResource())..", by: "..getResourceInfo(
        getThisResource(), "author")..", v-"..getResourceInfo(
        getThisResource(), "version")..", is represented", plr)
end)
