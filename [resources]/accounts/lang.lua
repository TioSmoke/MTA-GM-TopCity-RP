

-- Definition of default language for this resource
r_lang = exports.core:getLanguage() or "en_US"

-- Language text storage for this resource
lang_txt = {
	-- English/English
	["en_US"] = {
		["msg_rental_shop"] = "Esta é uma loja de aluguel de onde você pode pegar um carro",
		["msg_fuel"] 		= "Todos os veículos precisam de combustível, você pode comprar combustível em qualquer posto de gasolina",
		["msg_skin_shop"] 	= "Esta é uma loja de skin, aqui você vai poder ficar mais estiloso ",
		["msg_gym"] 		= "Visite a academia para melhorar sua resistência ou músculos",
		["msg_health"] 		= "Você pode recuperar sua saúde comendo, literalmente, qualquer coisa",
		["msg_gang"] 		= "Crie/juntar-se a uma gangue pressionando F6 para abrir o painel de gangue/grupo/clã",
		["msg_gang2"] 		= "Quando você está em uma gangue você pode capturar turfe para ganhar dinheiro e respeito",
		["msg_work"] 		= "Este é um local de trabalho, você pode conseguir um emprego aqui para ganhar dinheiro",
		["msg_initialize"]	= "Iniciando...",
	},

	-- Swedish/Svenska
	["sv_SE"] = {
		["msg_rental_shop"] 	= "Här kan du hyra en bil för en fast timkostnad",
		["msg_fuel"] 		= "Alla fordon behöver bränsle, här kan du tanka bilen",
		["msg_skin_shop"] 	= "Detta är en skinshop, här kan du ändra utseende",
		["msg_gym"] 		= "Besök gymmet om du vill bygga muskler eller förbättra konditionen",
		["msg_health"] 		= "Du kan höja din hälsa genom att äta",
		["msg_gang"] 		= "Klicka på F6 för att öppna panelen där du kan skapa grupper och gäng",
		["msg_gang2"] 		= "Ta över och kontrollera områden för att tjäna pengar som gängmedlem",
		["msg_work"] 		= "Detta är ett arbete, här kan du söka jobb och tjäna pengar på hederligt vis",
		["msg_initialize"]	= "Initialiserar intro...",
	},
}
